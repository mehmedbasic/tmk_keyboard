#include <stdint.h>
#include <stdbool.h>
#include <avr/io.h>
#include <util/delay.h>
#include "print.h"
#include "debug.h"
#include "util.h"
#include "matrix.h"


#ifndef DEBOUNCE
#   define DEBOUNCE    5
#endif
static uint8_t debouncing = DEBOUNCE;

/* matrix state(1:on, 0:off) */
static matrix_row_t matrix[MATRIX_ROWS];
static matrix_row_t matrix_debouncing[MATRIX_ROWS];
static uint8_t pin_map[] = {0, 1, 4, 5, 6, 7};

static void init_pins(void);

static void unselect_columns(void);

static void select_column(uint8_t row);

static matrix_row_t read_pin_f(uint8_t row);


inline
uint8_t matrix_rows(void) {
    return MATRIX_ROWS;
}

inline
uint8_t matrix_cols(void) {
    return MATRIX_COLS;
}

void matrix_init(void) {
    // JTAG disable for PORT F. write JTD bit twice within four cycles.
    MCUCR |= (1 << JTD);
    MCUCR |= (1 << JTD);

    // initialize row and col
    init_pins();
    unselect_columns();

    // initialize matrix state: all keys off
    for (uint8_t i = 0; i < MATRIX_ROWS; i++) {
        matrix[i] = 0;
        matrix_debouncing[i] = 0;
    }
}


uint8_t matrix_scan(void) {
    for (uint8_t rowIndex = 0; rowIndex < MATRIX_ROWS; rowIndex++) {
        matrix_row_t rowData = 0;
        for (uint8_t columnIndex = 0; columnIndex < MATRIX_COLS; columnIndex++) {
            select_column(columnIndex);
            _delay_us(30);

            matrix_row_t columnAsLong = (matrix_row_t) columnIndex;
            rowData |= read_pin_f(pin_map[rowIndex]) << columnAsLong;
        }

        if (matrix_debouncing[rowIndex] != rowData) {
            matrix_debouncing[rowIndex] = rowData;
            debouncing = DEBOUNCE;
        }

        unselect_columns();
        _delay_us(5);
    }

    _delay_ms(500);
    print("\nr/c 0123456789ABCDEFG\n");

    if (debouncing) {
        if (--debouncing) {
            _delay_ms(1);
        } else {
            print("Debounced\n");
            for (uint8_t i = 0; i < MATRIX_ROWS; i++) {
                matrix[i] = matrix_debouncing[i];
            }
            matrix_print();
        }
    }
    return 1;
}

bool matrix_is_modified(void) {
    if (debouncing) {
        return false;
    }
    return true;
}

inline
bool matrix_is_on(uint8_t row, uint8_t col) {
    matrix_row_t mask = matrix[row] & (1L << col);
    return mask == 0 ? 0 : 1;
}

inline
matrix_row_t matrix_get_row(uint8_t row) {
    return matrix[row];
}

void matrix_print(void) {
    print("\nr/c 0123456789ABCDEFG\n");

    for (uint8_t row = 0; row < MATRIX_ROWS; row++) {
        phex(row);
        xprintf(": ");

        for (uint8_t columnIndex = 0; columnIndex < MATRIX_COLS; columnIndex++) {
            xprintf("%d", matrix_is_on(row, columnIndex));
        }
        print("\n");
    }
}

uint8_t matrix_key_count(void) {
    uint8_t count = 0;
    for (uint8_t i = 0; i < MATRIX_ROWS; i++) {
        count += bitpop32(matrix[i]);
    }
    return count;
}

/* Columns 0 - 15
 *
 * 0  :  B 0
 * 1  :  B 1
 * 2  :  B 2
 * 3  :  B 3
 * 4  :  B 7
 *
 * 5  :  D 0
 * 6  :  D 1
 * 7  :  D 2
 * 8  :  D 3
 * 9  :  D 5
 * 10 :  D 4
 * 11 :  D 6
 * 12 :  D 7
 *
 * 13 :  B 4
 * 14 :  B 5
 * 15 :  B 6
 *
 * 16 :  C 6
 */
static void init_pins(void) {
    DDRB = 0b11111111;
    DDRD = 0b11111111;
    DDRC |= 1 << PC6;

    // Rows are input, set all DDRF bits to 0
    DDRF = 0b00000000;
    PORTF = 0b00000000;

    unselect_columns();
}

static void unselect_columns(void) {
    PORTB = 0b00000000;
    PORTD = 0b00000000;
    PORTC &= ~(1 << PC6);
}

static inline void select_b(uint8_t pin) {
    PORTB = (1 << pin);
}

static inline void select_d(uint8_t pin) {
    PORTD = (1 << pin);
}

static inline void select_c(uint8_t pin) {
    PORTC = (1 << pin);
}

static void select_column(uint8_t index) {
    switch (index) {
        case 0:
            select_b(PB0);
            break;
        case 1:
            select_b(PB1);
            break;
        case 2:
            select_b(PB2);
            break;
        case 3:
            select_b(PB3);
            break;
        case 4:
            select_b(PB7);
            break;
        case 5:
            select_d(PD0);
            break;
        case 6:
            select_d(PD1);
            break;
        case 7:
            select_d(PD2);
            break;
        case 8:
            select_d(PD3);
            break;
        case 9:
            select_d(PD5);
            break;
        case 10:
            select_d(PD4);
            break;
        case 11:
            select_d(PD6);
            break;
        case 12:
            select_d(PD7);
            break;
        case 13:
            select_b(PB4);
            break;
        case 14:
            select_b(PB5);
            break;
        case 15:
            select_b(PB6);
            break;
        case 16:
            select_c(PC6);
            break;
    }
}

static inline matrix_row_t read_pin_f(uint8_t pin) {
    return bit_is_set(PINF, pin) ? 1L : 0L;
}
